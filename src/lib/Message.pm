# -*- Perl -*-
#
#  Copyright (c) 1997-1998 Shawn T. Amundson <amundson@gimp.org>
#

package Message;

#use strict qw(refs subs);
use vars qw (@ISA @EXPORT %ON %OFF %OUT $redirect_stdout $redirect_stderr
	     $fork_and_date $pid $mail_on $mail_to $mail_command $mesg_init);

require Exporter;
@ISA = qw(Exporter);
@EXPORT = (
	   # name                       brief description
	   # --------------------------------------------------------
	   'mesg',
	   'mesg_init',
	   'mesg_quit',
	   'mesg_capture_and_mail'
	   );

%ON = ( 
       'ERROR'   => 1,
       'DEBUG'   => 0,
       'VERBOSE' => 1,
       'WARNING' => 1,
       'MAIL'    => 0
       );

%OUT = (
	'ERROR'   => 'STDERR',  
	'DEBUG'   => 'STDERR',
	'VERBOSE' => 'STDOUT',
	'WARNING' => 'STDERR',
	'MAIL'    => 'foo'
	);


$redirect_stdout = 1;
$redirect_stderr = 2;
$fork_and_date = 1;
$mesg_init = 0;

$mail_on = 0;
$mail_to = "";
$mail_command = "";
$mail_capture_only = 0;	

# We need this to print out the appropriate text.  Perhaps this
# should be defined globably and available elsewhere, but for now
# this will do.
my(@months) = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	    
if (0) {
    # THIS GETS RID OF A WARNING
    print SVOUT "";
    print SVERR "";
}

sub mesg_capture_and_mail {
    $mail_command = shift;
    $mail_to = shift;
    $mail_on = 1;

    set_on('MAIL');
    set_out('MAIL','MAILOUT');

}

sub mesg_init {

    $mesg_init = 1;

    if ($mail_on) {
	my $waitedpid;
	
	pipe(MAILIN,MAILOUT);

      MAILFORK:
	
	if ($mpid = fork) {
	    
	    # We don't read in any process but the one forked here.
	    close(MAILIN);
	    
	    # We need to set the default to non-buffering.  We might actually
	    # wish to redirect STDOUT later, but for now we will assume we don't
	    # need to.
	    select(MAILOUT); $| = 1;

	} elsif (defined $mpid) {
	    
	    # We don't write ANY output in this child.
	    close(MAILOUT);
	
	    open(MAIL,"|$mail_command $mail_to") || die "Could not open $mail_command: $!";
	    select(MAIL); $| = 1; select(STDOUT);

	    my($the_time,$sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
	    my($ftime,$t);
	    while(<MAILIN>) {
		$t = time;
		($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($t);
		$ftime = sprintf("%2.2d:%2.2d:%2.2d",$hour, $min, $sec);

		if ($fork_and_date) {
		    printf(MAIL "%3.3s %2d $ftime $_", $months[$mon], $mday);
		} else {
		    print MAIL $_;
		}
	    }

	    close(MAIL);
	    close(MAILIN);
	    exit(0);

	} else {
	    die "****MAILFORK fork didn't work  bailing out...\n";
	}
    }


    if ($fork_and_date || $mail_on) {

	my $waitedpid;
	
	pipe(PIPEIN,PIPEOUT);

      OUTPUTFORK:
	
	if ($pid = fork) {
	    
	    # We don't read in any process but the one forked here.
	    close(PIPEIN);
	    
	    # We need to set the default to non-buffering.  We might actually
	    # wish to redirect STDOUT later, but for now we will assume we don't
	    # need to.
	    select(PIPEOUT); $| = 1;

	    if ($redirect_stdout) {

		open(SVOUT, ">&STDOUT") || die "Can't dup stdout";
		open(STDOUT, ">&PIPEOUT") || die "Can't redirect stdout";
	    } 
	  	    
	    if ($redirect_stderr) {
		open(SVERR, ">&STDERR") || die "Can't dup stderr";
		open(STDERR, ">&PIPEOUT") || die "Can't redirect stderr";
	    }

	} elsif (defined $pid) {
	    
	    # We don't write ANY output in this child.
	    close(PIPEOUT);

	    # Read our PIPEIN pipe and attach standard info about time.  This is 
	    # really the entire point of this child process.
	    my($the_time,$sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
	    my($ftime,$t);
	    while (<PIPEIN>) {
		$t = time;
		($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($t);
		$ftime = sprintf("%2.2d:%2.2d:%2.2d",$hour, $min, $sec);

		if ($mail_on) {
		    if ($fork_and_date) {
			printf(STDOUT "%3.3s %2d $ftime $_", $months[$mon], $mday) if (!$mail_capture_only);
		    } else {
			printf(STDOUT "$_") if (!$mail_capture_only);
		    } 
		    printf(MAILOUT "$_");
		} else {
		    if ($fork_and_date) {
			printf(STDOUT "%3.3s %2d $ftime $_", $months[$mon], $mday);
		    } else {
			printf(STDOUT "$_");
		    }
		}
	    }
	    
	    # We are done with the client because the parent and all other 
	    # children have closed the PIPEOUT file handle.  That means exit.
	    close(MAILOUT) if ($mail_on);
	    close(PIPEIN);

	    exit(0);
	    
	} else {
	    die "**** OUTPUTFORK fork didn't work  bailing out...\n";
	}
	  
  }
	
}

sub mesg_quit {

    return if (!$mesg_init);

    if ($fork_and_date || $mail_on) {

	if ($redirect_stderr) {
	    open(STDERR, ">&SVERR") || die "Can't replace stderr!";
	}
	if ($redirect_stdout) {
	    open(STDOUT, ">&SVOUT") || die "Can't replace stdout!";
	}

	my $waitedpid;
	
	# We are done running what we have to run; all we want to do now is
	# close PIPEOUT which should make our child forked here exit and then
	# we want to wait for it to do this.
	close(PIPEOUT);
	$waitedpid = wait;
	
	if ($waitedpid != $pid) {
	    die "At end of execution, pid $pid != waitedpid $pid";
	}

	if ($mail_on) {
	    close(MAILOUT);
	    $waitedpid = wait;

	    if ($waitedpid != $mpid) {
		die "At end of execution, pid $pid != waitedpid $pid";
	    }
	}

    }

}

sub mesg {
    my $mode = shift;
    my $format = shift;
    my @args = @_;

    if (!$ON{$mode} && !$OUT{$mode}) {
	print STDERR "WARNING: Message::mesg: Don't know mode $mode\n"; 
	print STDERR "WARNING: Message::mesg: Creating mode $mode...\n";
	$ON{$mode} = 1;
	$OUT{$mode} = 'STDERR';
    }

    my ($call,$filename,$line) = caller;
    #print "$call $filename $line $ON{$mode} $OUT{$mode}\n";

    if ($ON{$mode}) {
	my $where = $OUT{$mode};
	if ($mode eq "VERBOSE" || $mode eq "MAIL") {
	    print $where "$format",@args;
	} elsif ($mode eq "DEBUG") {
	    if ($ON{"DEBUG_ALL"} || $ON{"DEBUG_$call"}) {
		printf($where "$mode: $call (line $line): $format", @args);
	    }
	} else {
	    printf($where "$mode: $call (line $line): $format", @args);
	}
    }
}

sub set_on {
    my $mode = shift;

    if ($mode eq "DEBUG") {
	my @debug = @_;
	foreach(@debug) {
	    if ($_ eq "ALL" || $_ eq "all") {
		$ON{DEBUG_ALL} = 1;
	    } else {
		$ON{"DEBUG_$_"} = 1;
	    }
	}	
    }
    
    $ON{$mode} = 1;
}

sub set_off {
    my $mode = shift;
    $ON{$mode} = 0;
}

sub set_out {
    my $mode = shift;
    my $out = shift;
    $OUT{$mode} = $out;
}

1;

