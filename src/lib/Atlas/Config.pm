# -*- Perl -*-
#
# Atlas::Config.pm
# Copyright (C) 1997 Shawn T. Amundson <amundson@cs.umn.edu>
#

package Atlas::Config;

use strict qw(subs vars);
require 5.002;

use vars qw(@ISA @EXPORT %MAJOR_BLOCKS %KEYS_QUEUE %KEYS_GLOBAL
	    $Error $Debug);

require Exporter;
@ISA = qw(Exporter);
@EXPORT = ( );

#
# MAJOR_BLOCKS defines what are valid blocks and if multiple instances
# of them can exist.  If multiple can exist, it is assumed that they are
# named.  Thus, in the config file, one that can exist multiple times
# might be defined like:
#   CLASS staff {
# Where as one that is single will only be:
#   FILES { 
#
# Data in this array is "m" for multiple, and "s" for single, or
# 'l' for a simple list.
#
%MAJOR_BLOCKS = ( 
		 'QUEUE'    => 'm',
		 'GLOBAL'   => 's',
		 );
#
# Now the KEYS are defined for each of the above blocks.
#
# This is done via an associative array, where the data
# is the seperator.  A seperator of '' means there can only
# be one data, and multiple instances is an error.
#
# The data is actually a list split on |, so | is a special 
# character which can not be used as a seperator in the config
# file.
#
# Also, all white space is converted to a single space, so " "
# can be used here to represent a split on all white space.
#

%KEYS_GLOBAL = (
		'admin'   => '',
		'mail_server'  => '',
                'smtp_server'  => '',
                'top_level'    => '',
		'path_to_perl' => ''
	        );

%KEYS_QUEUE = (
	     	'title' => '',
 		'initial_autoreply' => ''
	       );

sub new {
    my $type = shift;
    my $config = shift;

    my $self = {};
    bless $self;

    $self->{'config'} = $config;

    return $self->parse_config;
};

sub parse_config {
    my $self = shift;
    my $config = $self->{'config'};
    my($success,$error,$line,$state);
    my(@current_block,$CURR_BLOCK,$tmpvar,$variable,$fkey,$fdata,$array);
    my(@splitters,$sp);

    $success = open(CONFIG,"$config");
    if (!$success) {
	$Error = "could not open config file $config";
	return undef;
    }

    while ($line = <CONFIG>) {
	chop($line);
	$line = &format_line($line);
	next if ($line eq "");
	
	mesg('DEBUG', "Processing: $line\n");
	
	if ($line !~ /^.* \{$/) {
	    $Error = "error in config file, incorrect format for header of block: $line";
	    return undef;
	}
	    
	$line =~ s/ \{$//;
	@current_block = split(/ /,$line);

	mesg('DEBUG',"@current_block\n");

	if (!defined($MAJOR_BLOCKS{$current_block[0]})) {
	    $Error = "in config, not a valid block: $current_block[0]";
	    return undef;
	}
	
	$CURR_BLOCK = $current_block[0];

	if ($MAJOR_BLOCKS{$current_block[0]} eq "m") {
	    $tmpvar = "CONF_$current_block[0]";
	    push(@$tmpvar,$current_block[1]);
	}

	$variable = "CONF_$line";
	$variable =~ s/ /_/g;
	mesg('DEBUG',"Using variable: `$variable'\n");

	$line =~ s/ \{//g;
	   
	$line = <CONFIG>;
	$line = &format_line($line);
	while ($line ne "}") {
	    $fkey = $line;
	    $fdata = $line;
	    $fkey =~ s/\s+.*$//g;
	    if ($fkey eq "") {
		$line = <CONFIG>;
		$line = &format_line($line);
		next;
	    }

	    if ($MAJOR_BLOCKS{$CURR_BLOCK} eq 'm' 
		|| $MAJOR_BLOCKS{$CURR_BLOCK} eq 's') {

		$fdata =~ s/^\w+//;
		$fdata =~ s/^\s+//g;

		$array = "KEYS_$CURR_BLOCK";
		if (defined $$array{$fkey}) {
		    mesg('DEBUG',"KEY $fkey checks out...\n");
		    
		    if ($$array{$fkey} eq '') {
			if (defined $$variable{$fkey}) {
			    $Error = "key defined multiple times: $fkey";
			    return undef;
			}
			$$variable{$fkey} = $fdata;
		    } else {
			@splitters = split(/\|/,$$array{$fkey});
			foreach $sp (1..$#splitters) {
			    $fdata =~ s/$splitters[$sp]/$splitters[0]/g;
			}
			if (defined $$variable{$fkey}) {
			    $$variable{$fkey} .= $splitters[0];
			    $$variable{$fkey} .= $fdata;
			} else {
			    $$variable{$fkey} = $fdata;
			}
		    }
		    mesg('DEBUG',"KEY: $fkey  DATA: $$variable{$fkey}\n");
		} else {
		    $Error = "key $fkey invalid in block $CURR_BLOCK";
		    return undef;
		    
		}

	    } else {
		mesg('DEBUG',"Pushing $fdata onto $variable\n");
		push(@$variable,$fdata);
	    }

	    $line = <CONFIG>;
	    $line = &format_line($line);
	    
	}
	
    }
    close(CONFIG);

    &mesg('DEBUG', "Data recieved from config:\n");
    &mesg('DEBUG', "--------------------------\n");
    
    my($block,$var,$k,$n);
    foreach $block (keys %MAJOR_BLOCKS) {
	&mesg('DEBUG', "Major Block: $block (type $MAJOR_BLOCKS{$block})\n");
	$tmpvar = "CONF_$block";
	if ($MAJOR_BLOCKS{$block} eq "m") {
	    foreach (0..$#$tmpvar) {
		&mesg('DEBUG', "  Minor Block: $$tmpvar[$_]\n");
		$var = "CONF_" . $block . "_" . $$tmpvar[$_];
		foreach $k (keys %$var) {
		    mesg('DEBUG',"    -->%-10s %s\n",$k,$$var{$k});
		}
	    }
	} elsif ($MAJOR_BLOCKS{$block} eq "s") {
	    foreach $k (keys %$tmpvar) {
		mesg('DEBUG',"  -->%-10s %s\n",$k,$$tmpvar{$k});
	    }
	} elsif ($MAJOR_BLOCKS{$block} eq "l") {
	    foreach $n (0..$#$tmpvar) {
		mesg('DEBUG',"  -->%s\n",$$tmpvar[$n]);
	    }
	} else {
	    $Error = "error in major block array.  $MAJOR_BLOCKS{$block} invalid";
	    return undef;
	}
    }
    return $self;
}

sub format_line {
    my $line = shift;
    #$line =~ s/\s+/ /g;
    $line =~ s/#.*//g;
    $line =~ s/^\s+//;
    $line =~ s/\s+$//g;
    $line;
}

#
# Error returns:
#   0  Everything good.
#   1  Key not defined in block
#   2  Not valid key for block
#   3  Block not defined (list)
#  99  Totally fucked up.
#
sub get_info {
    my $self = shift;
    my $block = shift;
    
    if (!defined $MAJOR_BLOCKS{$block}) {
	$Error = "block not defined: $block";
	return undef;
    }

    my $var = "CONF_$block";

    if ($MAJOR_BLOCKS{$block} eq "s") {
	
	my $key = shift;
	my $varval = "KEYS_$block";
	if (!defined $$varval{$key}) {
	    $Error = "key $key is not valid for block type $block";
	    return undef;
	}

	if (!defined $$var{$key}) {
	    $Error = "key $key is not defined (in config file) for block type $block";
	    return undef;
	}

	mesg('DEBUG',"Got info: $$var{$key} for key $key\n");
	return (0,$$var{$key});
    } elsif ($MAJOR_BLOCKS{$block} eq "l") {
	if (!defined @$var) {
	    $Error = "list block $block is not defined in config file";
	    return undef;
	}
	return @$var;
    } elsif ($MAJOR_BLOCKS{$block} eq "m") {
	my $minblock = shift;
	my $key = shift;
	
	my $array = $var . "_" . $minblock;
	if (!defined %$array) {
	    $Error = "$block $minblock not defined in config";
	    return undef;
	}

	my $k = $$array{$key};
	mesg('DEBUG',"Got data $k for key $key\n");
	
	my $var_keys = "KEYS_$block";
	if (!defined $$var_keys{$key}) {
	    $Error = "no such key $key in block $block";
	    return undef;
	}

	my @splitters = split(/\|/,$$var_keys{$key});
	if ($#splitters > -1) {
	    mesg('DEBUG',"splitter: '$splitters[0]'\n");
	    my @stuff = split(/$splitters[0]/,$k);
	    return @stuff;
	} else {
	    return $k;
	}
    }

    $Error = "error in MAJOR_BLOCKS definition or call to get_info; block $block";
    return undef;
}

sub get_minor_blocks {
    my $self = shift;
    my $block = shift;

    if (!defined $MAJOR_BLOCKS{$block}) {
	$Error = "block $block is not a MAJOR_BLOCK";
	return undef;
    } elsif ($MAJOR_BLOCKS{$block} ne "m") {
	$Error = "block $block is not defined as a multiple block!";
	return undef;
    }

    my $var = "CONF_$block";

    return @$var;
    
}

sub mesg {
  my $tag = shift;
  my $mesg = shift;
  printf("$tag: $mesg",@_) if $Debug; 
}

1;

