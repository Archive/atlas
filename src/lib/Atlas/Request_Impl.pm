package Atlas::Request_Impl;

use strict;

@Atlas::Request_Impl::ISA = qw(Atlas::Request);

sub new {
    my ($class) = @_;
    my $self = bless {}, $class;

    $self->{'request_id'} = 0;
    $self->{'subject'} = '';
    $self->{'_status'} = 'Yeah';

    $self;
}

sub set_status {
    my $self = shift;
    my $status = shift;

    $self->{'_status'} = $status;
}

sub get_status {
    my $self = shift;
    return $self->{'_status'};
}

